import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { map } from "rxjs/operators";

@Injectable()
export class HeroesService {
  heroesURL: string = "http://gateway.marvel.com/v1/public/characters?ts=54&apikey=1130794682294d1851f04d1221c508f0&hash=f699d19bc56efade82af9978c305dc90";
  private heroes: Heroe[] = [];
  //  private historieta: string[];

  constructor(private http: Http) {
    this.http.get(this.heroesURL).map(res => res.json()).subscribe(data => {
      console.log("---data", data.data);

      for (let i = 0; i < data.data.count; i++) {
        let elheroe: Heroe;
        //        this.historieta = [];
        /*
                for (let j = 0; j < data.data.results[i].comics.items; j++) {
                  this.historieta[j] = data.data.results[i].comics.items[j].name;
                }
        
                console.log("comics", this.historieta);
        
                elheroe = this.heroeNuevo(data.data.results[i].name, data.data.results[i].description, data.data.results[i].modified, data.data.results[i].thumbnail.path + "." + data.data.results[i].thumbnail.extension, this.historieta);
        */

      

        elheroe = this.heroeNuevo(data.data.results[i].name, data.data.results[i].description, data.data.results[i].modified, data.data.results[i].thumbnail.path + "." + data.data.results[i].thumbnail.extension,data.data.results[i].comics.items );

        this.heroes.push(elheroe);
      }
    });
  }
  heroeNuevo(name, description, modified, thumbnail, _comics): Heroe {
    return {
      nombre: name,
      bio: description,
      aparicion: modified,
      img: thumbnail,
      casa: 'Marvel',
      comics: _comics
    };
  }

  getHeroes(): Heroe[] {
    return this.heroes;
  }

  getHeroe(id: number): Heroe {
    return this.heroes[id];
  }

  buscarHeroes(termino: string): Heroe[] {
    let heroeArr: Heroe[] = [];
    termino = termino.toLowerCase();

    //for (let heroe of this.heroes){
    for (let i = 0; i < this.heroes.length; i++) {
      let heroe = this.heroes[i];
      let nombre = heroe.nombre.toLowerCase();
      if (nombre.indexOf(termino) >= 0) {
        heroe.idx = i;
        heroeArr.push(heroe)
      }
    }
    return heroeArr;
  }
}

export interface Historieta {
  link: string;
  titulo: string;
}

export interface Heroe {
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  casa: string;
  idx?: number;
  comics: Historieta[];
}